import pymysql
import Config

def find_shingle_in_database(word):
    sql = """SELECT * FROM TFIDF_table
    WHERE Word='{word}'
    """.format(word=word)
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, Config.db_name)
    cursor = db.cursor()
    cursor.execute(sql)
    result = cursor.fetchone()
    if result:
        _,_,_,shinglecount,shingle_tfidf = result
        return shinglecount,shingle_tfidf
    else:
        return None,None

def hamming2(x, y):
    """Calculate the Hamming distance between two bit strings"""
    assert len(x) == len(y)
    count, z = 0, int(x, 2) ^ int(y, 2)
    while z:
        count += 1
        z &= z - 1  # magic!
    return count

def binary_conversion_for_sim_hash(x):
    if int(x) >= 1:
        return 1
    elif int(x) <= 0:
        return 0

def binary_conversion_for_hash(x, tfidf):
    if int(x) == 1:
        return 1*tfidf
    elif int(x) == 0:
        return -1*tfidf

def bin_string_to_list(bin_str, tfidf):
    bin_list = [binary_conversion_for_hash(d, tfidf) for d in bin_str[2:]]
    # Because our hash is always 128 bits and it'c constant
    num_of_bits = 128 - len(bin_list)
    add_list = [-1*tfidf]*num_of_bits
    # print(len(add_list + bin_list))
    return add_list + bin_list