import pymysql
import Config
import os
import math


def insert_into_tfidf_table(word, doc_list, word_count):
    # Open database connection

    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, Config.db_name)
    db.autocommit(True)

    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    execution_string = """SELECT * FROM {table} WHERE Word='{word}'""".format(
        table = Config.database_tf_idf_table,
        word=word
    )
    cursor.execute(execution_string)
    result = cursor.fetchone()

    if result is None:
        execution_string = """INSERT INTO {table} (Word,Docs,Total_Count) VALUES ('{word}','{docs}',{count})""".format(
            table=Config.database_tf_idf_table,
            word=word,
            docs=doc_list,
            count=word_count
        )
        cursor.execute(execution_string)
    else:
        id, word, files_list, total_count, tfidf_value = result
        files_list = files_list+','+doc_list
        total_count +=  word_count
        execution_string = """UPDATE {table} SET Word='{word}',Docs='{docs}',Total_Count={count} WHERE id={id}""".format(
            table=Config.database_tf_idf_table,
            word=word,
            docs=files_list,
            count=total_count,
            id=id
        )
        cursor.execute(execution_string)

    db.close()


def insert_tfidf(word, tfidf):
    tfidf = str(tfidf)
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, Config.db_name)
    db.autocommit(True)
    execution_string = """UPDATE {table} SET TF_IDF='{tfidf_value}' WHERE Word='{given_word}'""".format(
        table=Config.database_tf_idf_table,
        tfidf_value=tfidf,
        given_word=word
    )
    cursor = db.cursor()
    cursor.execute(execution_string)
    db.close()

def sum_of_word_count_in_tfidf_table():
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, Config.db_name)
    db.autocommit(True)
    execution_string = """SELECT SUM(Total_Count) FROM {table}""".format(
        table=Config.database_tf_idf_table
    )
    cursor = db.cursor()
    cursor.execute(execution_string)
    result = cursor.fetchone()[0]
    return result




def create_tfidf_table(db_name):
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, db_name)

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # Drop table if it already exist using execute() method.
    cursor.execute("DROP TABLE IF EXISTS {dbtable}".format(dbtable=Config.database_tf_idf_table))

    # Create table as per requirement
    sql = """CREATE TABLE {dbtable} (
        id MEDIUMINT NOT NULL AUTO_INCREMENT,
        Word VARCHAR(21844) NOT NULL,
        Docs VARCHAR(21844) NOT NULL,
        Total_Count INT NOT NULL,
        TF_IDF FLOAT,
        PRIMARY KEY (id)
       )""".format(dbtable=Config.database_tf_idf_table)

    cursor.execute(sql)

    # disconnect from server
    db.close()


def create_database(dbname):
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password)
    cursor = db.cursor()
    cursor.execute("CREATE DATABASE IF NOT EXISTS {databasename}".format(databasename=dbname))

def fill_tfidf_column(db_name):
    sql = """ SELECT * FROM TFIDF_table"""
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, db_name)

    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()
    sum_of_columns = sum_of_word_count_in_tfidf_table()
    for each_result in results:
        id,word,docs,totalcount,_ = each_result
        no_of_docs = len(docs.split(','))
        path, dirs, files = next(os.walk(Config.Docs_folder))
        file_count = len(files)
        tfidf = float(totalcount/sum_of_columns) * math.log(file_count/no_of_docs)
        insert_tfidf(word,tfidf)




if __name__ == "__main__":
    # create_database()
    ###### Test Code #########
    create_tfidf_table(Config.db_name)
    insert_into_tfidf_table('Test', 'file-1,file-2,file-3',45)
    insert_into_tfidf_table('Test-2', 'file-1,file-2,file-3',25)
    insert_into_tfidf_table('Test-2', 'file-4',5)
    insert_tfidf('Test',0.678)
    sum_of_word_count_in_tfidf_table()
