import Config
import string

shingleLength = Config.no_of_shingles


def exclude_punctuation(line):
    exclude = set(string.punctuation)
    s = ''.join(ch for ch in line if ch not in exclude)
    return s


def get_shingles(file_name):
    document = exclude_punctuation(open(file_name, 'r').read())
    tokens = document.split()
    for index,_ in enumerate(tokens):
        tokens[index] = tokens[index].lower()
    shingle_list = [tokens[i:i + shingleLength] for i in range(len(tokens) - shingleLength + 1)]
    # print(shingle_list) #Comment this in the end
    return shingle_list
