test_file_1 = 'file-1'
test_file_2 = 'file-2'
test_file_3 = 'file_3'
test_file_4 = 'file_4'
no_of_shingles = 1

############ Database Related Entries ##########

database_host = 'localhost'
db_username = 'root'
db_password  = 'rootpw'
db_name = 'MinHash'
database_tf_idf_table = 'TFIDF_table'
database_docs_list_table = 'Documents'
Docs_folder = 'docs_folder'