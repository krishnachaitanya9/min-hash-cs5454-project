import hashlib
import binascii

def get_md5_binary(line):
    hashcode = hashlib.md5(line.encode('utf-8')).hexdigest()
    return bin(int(hashcode, 16))


def get_md5_hexadecimal(line):
    hashcode = hashlib.md5(line.encode('utf-8')).digest()
    return hashcode
