import Config
import get_md5_hash
import shingles
import misc_tools
import numpy as np
import database_utils

file_1 = Config.test_file_1
file_2 = Config.test_file_2
file_3 = Config.test_file_3
file_4 = Config.test_file_4

def get_sim_hash(filename):
    shingles_list = shingles.get_shingles(filename)
    print('{file} gave {shingls} number of shingles'.format(file=filename, shingls=len(shingles_list)))
    bin_list = []
    shingle_dict = {}
    for every_shingle in shingles_list:
        shingle_sentence = ' '.join(every_shingle).strip()
        get_shingle_count = shingle_dict.get(shingle_sentence, 0)
        shingle_dict[shingle_sentence] = get_shingle_count + 1
    for every_shingle in shingles_list:
        shingle_sentence = ' '.join(every_shingle).strip()
        binary_number = get_md5_hash.get_md5_binary(shingle_sentence)

        # print(misc_tools.find_shingle_in_database(shingle_sentence))
        count = shingle_dict.get(shingle_sentence, 0)
        # count_from_db,tfidf = misc_tools.find_shingle_in_database(shingle_sentence)
        # total_count = database_utils.sum_of_word_count_in_tfidf_table()
        # if count is None:
        #     count = 0
        #     tfidf = 1
        # single_bin_list = misc_tools.bin_string_to_list(binary_number, tfidf)
        single_bin_list = misc_tools.bin_string_to_list(binary_number, count)
        bin_list.append(single_bin_list)
    bin_list = np.array(bin_list).astype(np.float)
    print(bin_list)
    sum_of_each_columns = np.sum(bin_list, axis=0)
    sim_hash_list = [misc_tools.binary_conversion_for_sim_hash(d) for d in sum_of_each_columns]
    sim_hash_string = ''.join(str(d) for d in sim_hash_list)
    print('Filename: ',filename,' Hash = ',sim_hash_string)
    return sim_hash_string

file_1_simhash = get_sim_hash(file_1)
file_2_simhash = get_sim_hash(file_2)
file_3_simhash = get_sim_hash(file_3)
file_4_simhash = get_sim_hash(file_4)


hamming_distance_1_2 = misc_tools.hamming2(file_1_simhash, file_2_simhash)
hamming_distance_2_3 = misc_tools.hamming2(file_2_simhash, file_3_simhash)
hamming_distance_3_4 = misc_tools.hamming2(file_3_simhash, file_4_simhash)
hamming_distance_4_1 = misc_tools.hamming2(file_4_simhash, file_1_simhash)
hamming_distance_1_3 = misc_tools.hamming2(file_1_simhash,file_3_simhash)
print('Hamming Distance between 1 and 2: ',hamming_distance_1_2)
print('Hamming Distance between 1 and 3: ',hamming_distance_1_3)
print('Hamming Distance between 2 and 3: ',hamming_distance_2_3)
print('Hamming Distance between 3 and 4: ',hamming_distance_3_4)
print('Hamming Distance between 4 and 1: ',hamming_distance_4_1)


