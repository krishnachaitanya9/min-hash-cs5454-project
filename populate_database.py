import misc_tools
import shingles
import os
import Config
import database_utils

filenames_list = []

def absoluteFilePaths(directory):
   for dirpath,_,filenames in os.walk(directory):
       for f in filenames:
           yield os.path.abspath(os.path.join(dirpath, f))

for every_file in absoluteFilePaths(Config.Docs_folder):
    filenames_list.append(every_file)

database_utils.create_database(Config.db_name)
database_utils.create_tfidf_table(Config.db_name)

shingle_dictionary = {}

for every_file in filenames_list:
    shingles_list = shingles.get_shingles(every_file)
    file_name = every_file.split('/')[-1]

    for every_shingle in shingles_list:
        shingle_word = ' '.join(every_shingle)
        shingle_count = shingle_dictionary.get(shingle_word,0)
        shingle_count += 1
        shingle_dictionary[shingle_word] = shingle_count
        database_utils.insert_into_tfidf_table(shingle_word,file_name,shingle_dictionary[shingle_word])

database_utils.fill_tfidf_column(Config.db_name)


